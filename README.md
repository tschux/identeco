# Identeco

Enterprise Identity Management System with nodeJS

## Description

![Rechte](https://gitlab.com/tschux/identeco/raw/master/doc/Berechtigungen.svg "user rights")

## Installation

```bash
npm install sails -g
git clone git@gitlab.com:tschux/identeco.git
cd identeco
npm install
sails lift
```

## Testing

```bash
npm test
```

### Test only jest

```bash
npm run jest
```



## Links

+ [Sails framework documentation](https://sailsjs.com/get-started)
+ [Deployment tips](https://sailsjs.com/documentation/concepts/deployment)
+ [Community support options](https://sailsjs.com/support)


