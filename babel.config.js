/**
 * Babel global config
 *
 * For more information see:
 *   https://babeljs.io/docs/en/config-files
 */

module.exports = {
  presets: [
    [
      '@babel/preset-env',
      {
        targets: {
          node: 'current',
        },
      },
    ],
  ],
};